package service;

import domain.DatatableRequestDomain;
import domain.DatatableResponseDomain;
import domain.Person;

import java.util.List;

public interface PersonService {

    List<Person> getPersonList();
    void deletePersonById(int id);
    Person addPerson(Person person);
    void updatePerson(Person person);
    Person getPersonById(int id);
    DatatableResponseDomain getAjaxResponse(DatatableRequestDomain datatableRequestDomain);



}
