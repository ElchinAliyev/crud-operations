package service.impl;

import domain.DatatableRequestDomain;
import domain.DatatableResponseDomain;
import domain.Person;
import factory.PersonRepositoryFactory;
import repository.PersonRepository;
import repository.impl.PersonRepositoryImpl;
import service.PersonService;

import java.util.List;

public class PersonServiceImpl implements PersonService {

    private PersonRepository personRepository = PersonRepositoryFactory.getPersonRepository();

    @Override
    public List<Person> getPersonList() {
        return null;
    }

    @Override
    public void deletePersonById(int id) {
        personRepository.deletePersonById(id);
    }

    @Override
    public Person addPerson(Person person) {
        return personRepository.addPerson(person);
    }

    @Override
    public void updatePerson(Person person) {
        personRepository.updatePerson(person);
    }

    @Override
    public Person getPersonById(int id) {
        return personRepository.getPersonById(id);
    }

    @Override
    public DatatableResponseDomain getAjaxResponse(DatatableRequestDomain datatableRequestDomain) {

        System.out.println("getAjaxResponse Method Worked ");
        DatatableResponseDomain datatableResponseDomain = new DatatableResponseDomain();
        datatableResponseDomain.setDraw(datatableRequestDomain.getDraw());


        List<Person> list = personRepository.getPersonListForDatatable(
                datatableRequestDomain.getSearchValue(),
                datatableRequestDomain.getColumnName(),
                datatableRequestDomain.getSortingDir(),
                datatableRequestDomain.getStart(),
                datatableRequestDomain.getLength()
        );
        String button = "<button class=\"view\" value=\"%s\" >View</button>" +
                "<button class=\"edit\" value=\"%s\" >Edit</button>" +
                "<button class=\"delete\" value=\"%s\">Delete</button>";
     //   String link = "<a href=\"/updateperson?id=%s\">Edit</a>" + "<a href=\"/deleteperson?id=%s\">Delete</a>" + "<a href=\"\" data-toggle=\"modal\" data-target=\"viewPerson\">View</a>";


        Object[][] data = new Object[list.size()][6];

        for (int i = 0; i < list.size(); i++) {
            System.out.println( );
            data[i][0] = list.get(i).getId();
            data[i][1] = list.get(i).getFullName();
            data[i][2] = list.get(i).getFatherName();
            data[i][3] = list.get(i).getContact();
            data[i][4] = list.get(i).getBirthDate().toString();
            data[i][5] = String.format(button, list.get(i).getId(), list.get(i).getId(), list.get(i).getId());

        }

        datatableResponseDomain.setData(data);
        datatableResponseDomain.setRecordsFiltered(personRepository.getFilteredPersonCount(datatableRequestDomain.getSearchValue()));
        datatableResponseDomain.setRecordsTotal(personRepository.getPersonCount());


        return datatableResponseDomain;
    }
}
