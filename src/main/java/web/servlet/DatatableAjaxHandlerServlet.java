package web.servlet;

import com.google.gson.Gson;
import domain.DatatableRequestDomain;
import domain.DatatableResponseDomain;
import service.PersonService;
import service.impl.PersonServiceImpl;
import util.DATATABLE_COLUMNS;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "DatatableAjaxHandlerServlet", urlPatterns = "/datatableajaxhandler")
public class DatatableAjaxHandlerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        int draw = Integer.parseInt(request.getParameter("draw"));
        int columnNumber = Integer.parseInt(request.getParameter("order[0][column]"));
        String sortDirection = request.getParameter("order[0][dir]");
        String searchValue = request.getParameter("search[value]");
        int start = Integer.parseInt(request.getParameter("start"));
        int length = Integer.parseInt(request.getParameter("length"));

        DatatableRequestDomain datatableRequestDomain = new DatatableRequestDomain();
        datatableRequestDomain.setDraw(draw);
        datatableRequestDomain.setColumnName(DATATABLE_COLUMNS.getColumnName(columnNumber).toString());
        datatableRequestDomain.setStart(start);
        datatableRequestDomain.setLength(length);
        datatableRequestDomain.setSortingDir(sortDirection);
        datatableRequestDomain.setSearchValue(searchValue);
        System.out.println(datatableRequestDomain);
        PersonService personService = new PersonServiceImpl();
        DatatableResponseDomain datatableResponseDomain = personService.getAjaxResponse(datatableRequestDomain);


        response.setContentType("application/json");
        String jsonData = new Gson().toJson(datatableResponseDomain);
        response.getWriter().print(jsonData);
    }
}
