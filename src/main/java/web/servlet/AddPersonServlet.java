package web.servlet;

import domain.Person;
import factory.PersonServiceFactory;
import service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(name = "AddPersonServlet", urlPatterns = "/addPersonServlet")
public class AddPersonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String fullName = request.getParameter("fullname");
        String fatherName = request.getParameter("fathername");
        LocalDate date = LocalDate.parse(request.getParameter("date"));
        String contact = request.getParameter("contact");

        Person person = new Person();
        person.setFullName(fullName);
        person.setFatherName(fatherName);
        person.setContact(contact);
        person.setBirthDate(date);
        PersonService personService = PersonServiceFactory.getPersonService();
        personService.addPerson(person);
        response.sendRedirect(getServletContext().getContextPath() + "/datatable");


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
