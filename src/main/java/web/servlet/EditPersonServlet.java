package web.servlet;

import domain.Person;
import factory.PersonServiceFactory;
import service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;

@WebServlet(name = "EditPersonServlet", urlPatterns = "/editPersonServlet")
public class EditPersonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));
        String fullName = request.getParameter("fullname");
        String fatherName = request.getParameter("fathername");
        LocalDate date = LocalDate.parse(request.getParameter("date"));
        String contact = request.getParameter("contact");
        Person person = new Person();
        person.setId(id);
        person.setFullName(fullName);
        person.setFatherName(fatherName);
        person.setContact(contact);
        person.setBirthDate(date);
        PersonServiceFactory.getPersonService().updatePerson(person);
        response.sendRedirect(request.getContextPath()+"/datatable");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
