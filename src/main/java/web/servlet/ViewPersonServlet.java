package web.servlet;

import com.google.gson.Gson;
import domain.Person;
import factory.PersonServiceFactory;
import service.PersonService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ViewPersonServlet", urlPatterns = "/viewperson")
public class ViewPersonServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int id = Integer.parseInt(request.getParameter("id"));

        PersonService personService = PersonServiceFactory.getPersonService();

        Person person = personService.getPersonById(id);

        response.setContentType("application/json");
        String data = new Gson().toJson(person);

        response.getWriter().print(data);

    }
}
