package domain;

import java.io.Serializable;

public class DatatableRequestDomain implements Serializable {
    private static final long serialVersionUID = 5209833007218653377L;
    private int draw;
    private int start;
    private int length;

    public DatatableRequestDomain() {
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public String getColumnName() {
        return columnName;
    }

    @Override
    public String toString() {
        return "DatatableRequestDomain{" +
                "draw=" + draw +
                ", start=" + start +
                ", length=" + length +
                ", columnNumber=" + columnName +
                ", searchValue='" + searchValue + '\'' +
                ", sortingDir='" + sortingDir + '\'' +
                '}';
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public String getSortingDir() {
        return sortingDir;
    }

    public void setSortingDir(String sortingDir) {
        this.sortingDir = sortingDir;
    }

    private String columnName;
    private String searchValue;
    private String sortingDir;
}
