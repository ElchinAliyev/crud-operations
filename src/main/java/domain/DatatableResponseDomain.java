package domain;

import java.io.Serializable;
import java.util.Arrays;

public class DatatableResponseDomain  implements Serializable {
    private static final long serialVersionUID = 1826133695752651341L;
    private int draw;
    private int recordsTotal;

    public DatatableResponseDomain() {
    }

    @Override
    public String toString() {
        return "DatatableResponseDomain{" +
                "draw=" + draw +
                ", recordsTotal=" + recordsTotal +
                ", recordsFiltered=" + recordsFiltered +
                ", data=" + Arrays.toString(data) +
                '}';
    }

    public int getDraw() {
        return draw;
    }

    public void setDraw(int draw) {
        this.draw = draw;
    }

    public int getRecordsTotal() {
        return recordsTotal;
    }

    public void setRecordsTotal(int recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    public int getRecordsFiltered() {
        return recordsFiltered;
    }

    public void setRecordsFiltered(int recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    public Object[][] getData() {
        return data;
    }

    public void setData(Object[][] data) {
        this.data = data;
    }

    private int recordsFiltered;
    private Object[][] data;

}
