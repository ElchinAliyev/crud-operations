package domain;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@NamedNativeQuery(
        name = "getPersonCount",
        query = "select count(id) from Person"
)

@NamedNativeQuery(
        name = "getFilteredCountOfPersons",
        query = "select count(id) from Person where concat(id,birthdate,IFNULL(father_name,''),contact,full_name) " +
                " LIKE :searchValue "
)

@Entity
@Table(name = "Person")
public class Person implements Serializable {
    private static final long serialVersionUID = -3278759106009633176L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    public Person(int id, String fullName, String fatherName, LocalDate birthDate, String contact) {
        this.id = id;
        this.fullName = fullName;
        this.fatherName = fatherName;
        this.birthDate = birthDate;
        this.contact = contact;
    }


    @Column(name = "full_name")
    private String fullName;
    @Column(name = "father_name")
    private String fatherName;
    @Column(name = "birthdate")

    private LocalDate birthDate;

    @JoinTable
    @JoinColumn()
    private String contact;


    public Person() {
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", fatherName='" + fatherName + '\'' +
                ", birthDate=" + birthDate +
                ", contact='" + contact + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
