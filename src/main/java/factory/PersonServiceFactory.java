package factory;

import service.PersonService;
import service.impl.PersonServiceImpl;

public class PersonServiceFactory {
    private static PersonService personService = null;

    public static PersonService getPersonService() {


        if (personService == null) {
            personService = new PersonServiceImpl();
            return personService;
        } else {

            return personService;
        }


    }


}
