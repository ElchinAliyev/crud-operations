package factory;

import repository.PersonRepository;
import repository.impl.PersonRepositoryImpl;

public class PersonRepositoryFactory {
    private static PersonRepository personRepository = null;


    public static PersonRepository getPersonRepository() {
        if (personRepository == null) {
            personRepository = new PersonRepositoryImpl();
            return personRepository;
        } else {
            return personRepository;
        }

    }
}
