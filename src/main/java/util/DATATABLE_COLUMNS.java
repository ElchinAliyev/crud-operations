package util;

public enum DATATABLE_COLUMNS {
    id(0),
    full_name(1),
    father_name(2),
    contact(3),
    birthdate(4);

    private int value;

    DATATABLE_COLUMNS(int value) {
        this.value = value;
    }


    public static DATATABLE_COLUMNS getColumnName(int value){
        if (value == 0){
            return DATATABLE_COLUMNS.id;
        }
        else if (value==1){
            return DATATABLE_COLUMNS.full_name;
        }
        else if (value==2){
            return DATATABLE_COLUMNS.father_name;
        }
        else if (value==3){
            return DATATABLE_COLUMNS.contact;
        }
        else if (value==4){
            return DATATABLE_COLUMNS.birthdate;
        }
        else {

            throw new RuntimeException("Such Column Doesn't exist" + value);
        }



    }
}
