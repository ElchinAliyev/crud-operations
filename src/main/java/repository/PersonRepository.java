package repository;

import domain.DatatableRequestDomain;
import domain.DatatableResponseDomain;
import domain.Person;

import java.util.List;

public interface PersonRepository {


    List<Person> getPersonListForDatatable(String searchValue , String columnName , String columnDir, int start , int offset);
    void deletePersonById(int id);
    Person addPerson(Person person);
    void updatePerson(Person person);
    Person getPersonById(int id);
    int getPersonCount();
    int getFilteredPersonCount(String searchValue);

}
