package repository.impl;

import domain.Person;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import repository.PersonRepository;
import util.HibernateUtil;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class PersonRepositoryImpl implements PersonRepository {


    @Override
    public List<Person> getPersonListForDatatable(String searchValue, String columnName, String columnDir, int start, int offset) {


        Session session = HibernateUtil.getSession();

        String sql = "select id , full_name , father_name ,birthdate, contact from person p " +
                " where concat(id,birthdate,IFNULL(father_name,''),contact,full_name) LIKE :searchValue  order by %s %s " +
                " LIMIT %s ,%s ";

        sql = String.format(sql, columnName, columnDir, start, offset);

        NativeQuery<Person> query = session.createNativeQuery(sql, Person.class);
        query.setParameter("searchValue", "%" + searchValue + "%");
        List<Person> list = query.list();

        System.out.println(list);


        return list;
    }

    @Override
    public void deletePersonById(int id) {
        Person person = new Person();
        person.setId(id);
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(person);
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public Person addPerson(Person person) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        int x = (int) session.save(person);
        session.getTransaction().commit();
        session.close();
        person.setId(x);
        return person;
    }

    @Override
    public void updatePerson(Person person) {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(person);
        session.getTransaction().commit();
        session.close();

    }

    @Override
    public Person getPersonById(int id) {

        Session session = HibernateUtil.getSession();
        Person person = session.get(Person.class, id);
        session.close();
        return person;
    }

    @Override
    public int getPersonCount() {
        Session session = HibernateUtil.getSession();
        NativeQuery<BigInteger> nativeQuery = session.getNamedNativeQuery("getPersonCount");
        BigInteger bigInteger = nativeQuery.getSingleResult();
        int x = bigInteger.intValue();

        return x;

    }

    @Override
    public int getFilteredPersonCount(String searchValue) {
        Session session = HibernateUtil.getSession();
        NativeQuery<BigInteger> query = session.getNamedNativeQuery("getFilteredCountOfPersons");

        query.setParameter("searchValue", "%" + searchValue + "%");
        BigInteger bigInteger = query.getSingleResult();
        int x = bigInteger.intValue();

        return x;

    }


}
