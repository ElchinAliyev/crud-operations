<%--
  Created by IntelliJ IDEA.
  User: elchin
  Date: 02.02.2020
  Time: 20:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"/>
    <link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <title>Datatable</title>
</head>
<body>

<button id="addPerson">Add New Person</button>
<table id="myTable">
    <thead>
    <th>ID</th>
    <th>Full Name</th>
    <th>Father Name</th>
    <th>Contact</th>
    <th>Birthdate</th>
    <th>Actions</th>
    </thead>
    <tbody>

    </tbody>


</table>
<div class="modal fade" id="myView" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">View Person</h4>
            </div>
            <div class="modal-body">
                <form>
                    ID :<input id="viewId" type="text" value="" disabled/><br/>
                    Full Name :<input id="viewFullName" type="text" value="" disabled/><br/>
                    Father Name :<input type="text" id="viewFatherName" value="" disabled/><br/>
                    Birthdate :<input type="date" id="viewBirthdate" value="" disabled/><br/>
                    Contact :<input type="text" id="viewContact" value="" disabled/><br/>


                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="myEdit" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Edit Person</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/editPersonServlet">
                    <input id="editId" type="text" name="id" value="" hidden/>
                    Full Name :<input id="editFullName" type="text" name="fullname" value=""/><br/>
                    Father Name :<input type="text" id="editFatherName" name="fathername" value=""/><br/>
                    Birthdate :<input type="date" id="editBirthdate" name="date" value=""/><br/>
                    Contact :<input type="text" id="editContact" name="contact" value=""/><br/>

                    <input type="submit" value="Submit"/>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div class="modal fade" id="myAdd" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add Person</h4>
            </div>
            <div class="modal-body">
                <form method="post" action="/addPersonServlet">
                    Full Name :<input  type="text" name="fullname" value=""/><br/>
                    Father Name :<input type="text"  name="fathername" value=""/><br/>
                    Birthdate :<input type="date"  name="date" value=""/><br/>
                    Contact :<input type="text"  name="contact" value=""/><br/>

                    <input type="submit" value="Submit"/>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#myTable').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url": "/datatableajaxhandler",
                "async": false
            }
        });

        $(".view").click(function () {
            var x = $(this).val();
            console.log(x);

            $.ajax({

                method: 'GET',
                url: "/viewperson?id=" + x,
                success: function (data) {
                    $("#viewId").val(data.id);
                    $("#viewFullName").val(data.fullName);
                    $("#viewFatherName").val(data.fatherName);
                    var month ='';
                    var day ='';
                    if (data.birthDate.month<10){
                        month = '0'+data.birthDate.month;
                    }
                    else {
                        month = data.birthDate.month;
                    }
                    if (data.birthDate.day<10){
                        day = '0'+data.birthDate.day;
                    }
                    else {
                        day = data.birthDate.day;
                    }
                    var date = data.birthDate.year +'-'+month+'-'+ day;
                    $("#viewBirthdate").val(date);
                    $("#viewContact").val(data.contact);
                    $('#myView').modal('show');
                }


            });
        });
        $(".edit").click(function () {
            var x = $(this).val();
            console.log(x);

            $.ajax({

                method: 'GET',
                url: "/viewperson?id=" + x,
                success: function (data) {
                    $("#editId").val(data.id);
                    $("#editFullName").val(data.fullName);
                    $("#editFatherName").val(data.fatherName);
                    var month ='';
                    var day ='';
                    if (data.birthDate.month<10){
                        month = '0'+data.birthDate.month;
                    }
                    else {
                        month = data.birthDate.month;
                    }
                    if (data.birthDate.day<10){
                        day = '0'+data.birthDate.day;
                    }
                    else {
                        day = data.birthDate.day;
                    }
                    var date = data.birthDate.year +'-'+month+'-'+ day;
                $("#editBirthdate").val(date);
                    $("#editContact").val(data.contact);
                    $('#myEdit').modal('show');
                }
            });
        });
        $(".delete").click(function () {
            var x = $(this).val();
            if(confirm("Are you sure ?")) {

                $.ajax({
                    url: "/deleteperson?id=" + x,
                    method: 'POST',
                    success: function () {
                        window.location.replace(window.location.pathname + "/datatable");
                    }
                });
            }
        });
        $("#addPerson").click(function () {


        $("#myAdd").modal('show');






        });


    });

</script>
</body>
</html>
